jQuery(document).ready(function( $ ) {

	"use strict";

	// Menu effect
	$('.menu-btn-wrap ').on( 'click', function() {
		$('.menu-btn ').toggleClass('menu-btn-close');
		$('.main-menu-wrap').fadeToggle(500);
	});

	
	// Login effect
	$('.user-btn').on( 'click', function() {
		$('.login-form-wrap').fadeIn(500);
	});

	$('.login-close-btn').on( 'click', function() {	
		$('.login-form-wrap').fadeOut(500);
	});

	$('.register-btn').on( 'click', function() {	
		$('.login-form').css('display','none');
		$('.register-form').css('display','inline-block');
	});
	
	$('.register-back-btn').on( 'click', function() {	
		$('.login-form').css('display','inline-block');
		$('.register-form').css('display','none');
	});

	// Main Menu Sub Effect  
	$('.main-menu').find('li').click(function () {
		$('.main-menu').find('li').children('.sub-menu').stop().slideUp( 500 );
		$(this).children('.sub-menu').stop().slideToggle( 500 );	
	});

	// Left Menu Sub Effect 
	$('.left-menu').find('li').click(function () {
		$('.left-menu').find('li').children('.sub-menu').stop().slideUp( 500 );
		$(this).children('.sub-menu').stop().slideToggle( 500 );	
	});

	$(".left-menu .sub-menu-btn").parents('li').css("cursor", "pointer");

	// Carousel
	$('.main-carousel').owlCarousel({
		items: 1,
	    loop: true,
	    autoplay: true,
	    autoplayTimeout: 20000,
	    autoplayHoverPause:true,
	    nav: true,
		dots: true,
		navText: [
	      "<i class='fa fa-angle-left'></i>",
	      "<i class='fa fa-angle-right'></i>"
	      ]
	 
	});

	//  Tabs Effect
	 $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });

	// Fancybox
	$(".gallery-items").fancybox({
		'titlePosition' 	: 'over',
		'cyclic'			: true,
		'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
			return '<span id="fancybox-title-over">'+ (title.length ? ' &nbsp; <h3>' + title : '</h3>')  + '<p> სურათი ' +(currentIndex + 1) + ' / ' + currentArray.length +'</p>'+ '</span>';
		}
	});


}); // end dom ready